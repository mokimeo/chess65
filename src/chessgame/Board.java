package chessgame;


import java.util.ArrayList;

import java.util.StringTokenizer;

import piece.*;

/**
 * Board Class stores all the pieces, and tracking their locations on the board
 * as the game starts It checks whether the movement of pieces is legal It
 * implements all required functionalities
 * 
 * @author linhdang
 *
 */
 
 //Maury Johnson
 
 
public class Board {
	/**
	 * Create a chess board
	 */
	public static Piece[][] board = createBoard();
	/**
	 * Player'sturn
	 */
	public static int turn = 0;
	/**
	 * Rook position
	 */
	static int Rookpos = -1;
	/**
	 * old coordinate
	 */
	static int[] oldcoordinate = new int[2];
	/**
	 * new coordinate
	 */
	static int[] newcoordinate = new int[2];
	/**
	 * white king position
	 */
	public static int[] whitekingpos = new int[2];
	/**
	 * Black king position
	 */
	public static int[] blackkingpos = new int[2];
	/**
	 * Opponent coordinate who is checking White King
	 */
	public static int[] BlackChecking = new int[2];
	/**
	 * Opponent coordinate which is checking Black King
	 */
	public static int[] WhiteChecking = new int[2];
	/**
	 * List of valid pawn that can allow en passant move
	 */
	public static ArrayList<int[]> listap = new ArrayList<int[]>();
	/**
	 * Temporary board
	 */
	public static Piece[][] tempboard = createTempBoard();
	/**
	 * Indicates if White want to draw
	 */
	public static boolean WhiteDraw = false;
	/**
	 * Indicates if Black wants to draw
	 */
	public static boolean BlackDraw = false;
	/**
	 * Store promotion character
	 */
	public static String promotion = " ";
	/**
	 * Checking B opponent
	 */
	public static int[] Checking_B_Op = new int[2];
	/**
	 * Checking W opponent
	 */
	public static int[] Checking_W_Op = new int[2];

	/**
	 * Create the board with initial amount of black and white pieces for the
	 * initial board
	 * 
	 * @return a board
	 */
	private static Piece[][] createBoard() {
		Piece[][] tempboard = new Piece[9][9];
		String color = "   ";
		
		tempboard[0][8] = new EmptyPiece(-1, "  8");
		tempboard[1][8] = new EmptyPiece(-1, "  7");
		tempboard[2][8] = new EmptyPiece(-1, "  6");
		tempboard[3][8] = new EmptyPiece(-1, "  5");
		tempboard[4][8] = new EmptyPiece(-1, "  4");
		tempboard[5][8] = new EmptyPiece(-1, "  3");
		tempboard[6][8] = new EmptyPiece(-1, "  2");
		tempboard[7][8] = new EmptyPiece(-1, "  1");

		tempboard[8][0] = new EmptyPiece(-1, "  a");
		tempboard[8][1] = new EmptyPiece(-1, "  b");
		tempboard[8][2] = new EmptyPiece(-1, "  c");
		tempboard[8][3] = new EmptyPiece(-1, "  d");
		tempboard[8][4] = new EmptyPiece(-1, "  e");
		tempboard[8][5] = new EmptyPiece(-1, "  f");
		tempboard[8][6] = new EmptyPiece(-1, "  g");
		tempboard[8][7] = new EmptyPiece(-1, "  h");
		tempboard[8][8] = new EmptyPiece(-1, "  ");
		// team black initialize
		tempboard[0][0] = new Rook(1, " bR");
		tempboard[0][1] = new Knight(1, " bN");
		tempboard[0][2] = new Bishop(1, " bB");
		tempboard[0][3] = new Queen(1, " bQ");
		tempboard[0][4] = new King(1, " bK");
		tempboard[0][5] = new Bishop(1, " bB");
		tempboard[0][6] = new Knight(1, " bN");
		tempboard[0][7] = new Rook(1, " bR");

		tempboard[1][0] = new Pawn(1, " bP");
		tempboard[1][1] = new Pawn(1, " bP");
		tempboard[1][2] = new Pawn(1, " bP");
		tempboard[1][3] = new Pawn(1, " bP");
		tempboard[1][4] = new Pawn(1, " bP");
		tempboard[1][5] = new Pawn(1, " bP");
		tempboard[1][6] = new Pawn(1, " bP");
		tempboard[1][7] = new Pawn(1, " bP");

		// team white initialize

		tempboard[7][0] = new Rook(0, " wR");
		tempboard[7][1] = new Knight(0, " wN");
		tempboard[7][2] = new Bishop(0, " wB");
		tempboard[7][3] = new Queen(0, " wQ");
		tempboard[7][4] = new King(0, " wK");
		tempboard[7][5] = new Bishop(0, " wB");
		tempboard[7][6] = new Knight(0, " wN");
		tempboard[7][7] = new Rook(0, " wR");

		tempboard[6][0] = new Pawn(0, " wP");
		tempboard[6][1] = new Pawn(0, " wP");
		tempboard[6][2] = new Pawn(0, " wP");
		tempboard[6][3] = new Pawn(0, " wP");
		tempboard[6][4] = new Pawn(0, " wP");
		tempboard[6][5] = new Pawn(0, " wP");
		tempboard[6][6] = new Pawn(0, " wP");
		tempboard[6][7] = new Pawn(0, " wP");

		for (int i = 2; i <= 5; i++) {

			for (int j = 0; j < 8; j++) {

				color = getcolor(i, j);
				tempboard[i][j] = new EmptyPiece(-1, color);
				

			}
			
		}

		return tempboard;
	}

	/**
	 * Create a temporary board
	 * 
	 * @return a new temporary board
	 */
	public static Piece[][] createTempBoard() {
		Piece[][] tmpboard = new Piece[9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				tmpboard[i][j] = board[i][j];
			}
		}
		return tmpboard;
	}

	// is En Passant Able
	/**
	 * Check to see if the piece is in the list of initial-2-unit-space moves
	 * 
	 * @param x row
	 * @param y column
	 * @return boolean value corresponding if the piece is valid to do En passant
	 */
	public static boolean isEPAble(int x, int y) {
		for (int i = 0; i < listap.size(); i++) {
			
			if (listap.get(i)[0] == x && listap.get(i)[1] == y) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check to see if the tile is occupied by an actual piece
	 * 
	 * @param x row
	 * @param y column
	 * @return boolean value corresponding to the tile is being occupied
	 */
	public static boolean isOccupied(int x, int y) {
		if (board[x][y].name.equals(" ##") || board[x][y].name.equals("   ")) {
			return false;
		}
		return true;
	}

	/**
	 * Get color corresponding to the tile coordinate
	 * 
	 * @param i row
	 * @param j column
	 * @return the tile color corresponding to the tile based on its coordinate
	 */
	public static String getcolor(int i, int j) {
		if ((i % 2 == 0 && j % 2 == 1) || (i % 2 == 1 && j % 2 == 0)) {
			return " ##";
		}
		return "   ";
	}

	/**
	 * print out the board with proper pieces place in proper coordinate
	 */
	public static void initializeboard() {
		System.out.println();
		// System.out.println("----------------------------------");
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				System.out.print(board[i][j].name);
			}
			System.out.println();
		}

		// System.out.println("-----------------------------------");

	}

	/**
	 * Convert letter to integer
	 * 
	 * @param c letter
	 * @return integer corresponding to the letter
	 */
	public static int AlphaToInt(char c) {

		if (c == 'a')
			return 0;
		if (c == 'b')
			return 1;
		if (c == 'c')
			return 2;
		if (c == 'd')
			return 3;
		if (c == 'e')
			return 4;
		if (c == 'f')
			return 5;
		if (c == 'g')
			return 6;
		if (c == 'h')
			return 7;
		return -1;
	}

	/**
	 * Checking to see if the piece is trying to do a valid en passan move
	 * 
	 * @param color indicated player
	 * @param x1    current row
	 * @param y1    current column
	 * @param x2    new row
	 * @param y2    new column
	 * @return boolean value corresponding if its a valid en passant move
	 */
	public static boolean CheckEnPassant(int color, int x1, int y1, int x2, int y2) {

		if (y1 != y2 && Math.abs(y2 - y1) == 1 && Math.abs(x2 - x1) == 1) {
			if ((x2 - 1) > 0 && isOccupied(x2 - 1, y2)) {
				// System.out.println("check enpassant - 1" + board[x2 - 1][y2].name + " " +
				// board[x2 - 1][y2].color);

				if (board[x2 - 1][y2] instanceof Pawn) {

					if (color != board[x2 - 1][y2].color && isEPAble(x2 - 1, y2)) {

						return true;
					}

				}
			}

			if (x2 + 1 < 8 && isOccupied(x2 + 1, y2)) {
				// System.out.println("check enpassant + 1" + board[x2 + 1][y2].name + " " +
				// board[x2 + 1][y2].color);
				if (board[x2 + 1][y2] instanceof Pawn) {
					if (color != board[x2 + 1][y2].color && isEPAble(x2 + 1, y2)) {
						return true;
					}

				}
			}
		}

		return false;
	}

	/**
	 * Alternate player's turn
	 */
	public static void AlterTurn() {
		if (turn == 0) {
			turn = 1;
		} else {
			turn = 0;
		}
	}

	/**
	 * Checking if the rook is valid for Castling move
	 * 
	 * @param color indicated player
	 * @param x1    current row
	 * @param y1    current column
	 * @param x2    new row
	 * @param y2    new column
	 * @return boolean value corresponding to whether the Rook has moved
	 */
	public static boolean isValidRook(int color, int x1, int y1, int x2, int y2) {

		if (y2 > y1) {
			if (color == 0) {

				if (!Rook.RWRookMoved && board[7][7] instanceof Rook && board[7][7].color == color) {
					Rookpos = 1;
					return true;

				}
			} else {
				if (!Rook.RBRookMoved && board[0][7] instanceof Rook && board[0][7].color == color) {
					Rookpos = 2;
					return true;

				}

			}
		} else if (y2 < y1) {
			if (color == 0) {
				if (!Rook.LWRookMoved && board[7][0] instanceof Rook && board[7][0].color == color) {
					Rookpos = 3;
					return true;
				}
			} else {
				if (!Rook.LBRookMoved && board[0][0] instanceof Rook && board[0][0].color == color) {
					Rookpos = 4;
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Checks to see if the king of the given color is in check.
	 * 
	 * @param color player identification
	 * @return boolean value corresponding to the king being in check
	 */
	public static boolean isinCheck(int color) {
		int x = -1;
		int y = -1;
		if (color == 0) {
			x = whitekingpos[0];
			y = whitekingpos[1];

		} else {
			x = blackkingpos[0];
			y = blackkingpos[1];

		}
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (isOccupied(i, j)) {
					if (board[i][j].isValidMove(board[i][j].color, i, j, x, y) && board[i][j].color != color) {

						return true;
					}
				}
			}
		}
		return false;

	}

	/**
	 * print function to test the king position
	 */
	public static void printposking() {
		if (turn == 0) {
			System.out.println("white king pos : x: " + whitekingpos[0] + ",y : " + whitekingpos[1]);
		} else {
			System.out.println("black king pos : x: " + blackkingpos[0] + ",y : " + blackkingpos[1]);
		}
	}

	/**
	 * Announcing check when move is checking the opponent king
	 * 
	 * @param color player indentify
	 * @param x     row
	 * @param y     column
	 */
	public static void Check(int color, int x, int y) {
		if (color == 0) {
			if (board[x][y].isValidMove(color, x, y, blackkingpos[0], blackkingpos[1])) {
				WhiteChecking[0] = x;
				WhiteChecking[1] = y;
				System.out.println("CHECK! FROM WHITE PIECE");
			}
		} else {
			if (board[x][y].isValidMove(color, x, y, whitekingpos[0], whitekingpos[1])) {
				BlackChecking[0] = x;
				BlackChecking[1] = y;
				System.out.println("CHECK! FROM BLACK PIECE");
			}
		}
	}

	/**
	 * Check to see if the move is trying to take the piece that is checking its
	 * King
	 * 
	 * @param color player identification
	 * @param x2    destination row
	 * @param y2    destination column
	 * @return boolean value corresponding to move of the piece is taking the
	 *         opponent piece that is checking its King
	 */
	public static boolean KingProtected(int color, int x2, int y2) {

		if (color == 0) {
			if (x2 == BlackChecking[0] && y2 == BlackChecking[1]) {
				// System.out.println("KingProtected valid");
				return true;
			}
		} else {
			if (x2 == WhiteChecking[0] && y2 == WhiteChecking[1]) {
				// System.out.println("KingProtected valid");
				return true;
			}
		}
		return false;
	}

	/**
	 * Check to see if piece's move can block the opponent from checking its King
	 * 
	 * @param color player identification
	 * @param x1    current row
	 * @param y1    current column
	 * @param x2    new row
	 * @param y2    new column
	 * @return boolean value corresponding to the valid move of the piece to protect
	 *         its King given its color, its current position and its new position
	 */
	public static boolean BlockChecking(int color, int x1, int y1, int x2, int y2) {
		tempboard = createTempBoard();
		if (board[x1][y1].isValidMove(color, x1, y1, x2, y2)) {

			// System.out.println("BlockChecking: Valid move");
			board[x1][y1].Move(color, x1, y1, x2, y2);
		}
		if (!isinCheck(color)) {
			board = tempboard;
			return true;
		}

		board = tempboard;
		return false;
	}

	/**
	 * Doing castling move given the king position and rook position and their color
	 * 
	 * @param color player identification
	 * @param x1    current row
	 * @param y1    current column
	 * @param x2    new row
	 * @param y2    new column
	 */
	public static void MakeCastlingMove(int color, int x1, int y1, int x2, int y2) {

		String colortileking = getcolor(x1, y1);
		board[x1][y1] = new EmptyPiece(-1, colortileking);
		if (color == 0) {
			board[x2][y2] = new King(0, " wK");

		} else {
			board[x2][y2] = new King(1, " bK");
		}
		if (color == 0) {
			Board.whitekingpos[0] = x2;
			Board.whitekingpos[1] = y2;

		} else {
			Board.blackkingpos[0] = x2;
			Board.blackkingpos[1] = y2;
		}

		String colortile = "";
		switch (Rookpos) {
		case 1:
			// Right White Rook

			colortile = getcolor(7, 7);
			board[7][7] = new EmptyPiece(-1, colortile);
			board[x2][y2 - 1] = new Rook(0, " wR");
			break;
		case 2:
			// Right Black Rook
			colortile = getcolor(0, 7);
			board[0][7] = new EmptyPiece(-1, colortile);
			board[x2][y2 - 1] = new Rook(1, " bR");
			break;
		case 3:
			// Left White Rook
			colortile = getcolor(7, 0);
			board[7][0] = new EmptyPiece(-1, colortile);
			board[x2][y2 + 1] = new Rook(0, " wR");

			break;
		case 4:
			colortile = getcolor(0, 0);
			board[0][0] = new EmptyPiece(-1, colortile);
			board[x2][y2 + 1] = new Rook(1, " bR");

			// Left Black Rook
			break;
		default:
			break;
		}

	}

	/**
	 * making temporary King move
	 * 
	 * @param color player identification
	 * @param x1    current row
	 * @param y1    current column
	 * @param x2    new row
	 * @param y2    new column
	 */
	public static void tempKingMove(int color, int x1, int y1, int x2, int y2) {
		String colortile = getcolor(x1, y1);
		board[x1][y1] = new EmptyPiece(-1, colortile);
		if (color == 0) {
			board[x2][y2] = new King(0, " wK");

		} else {
			board[x2][y2] = new King(1, " bK");
		}

		if (color == 0) {
			whitekingpos[0] = x2;
			whitekingpos[1] = y2;
			// System.out.println("white king pos :x="+x2+",y="+y2);
		} else {
			blackkingpos[0] = x2;
			blackkingpos[1] = y2;
			// System.out.println("black king pos :x="+x2+",y="+y2);
		}

	}

	/**
	 * Check to see if The king's move is through check before doing the castling
	 * 
	 * @param color player identification
	 * @param x1    current row
	 * @param y1    current column
	 * @param x2    new row
	 * @param y2    new column
	 * @return boolean value corresponding if The king is going through check
	 */
	public static boolean isThroughCheck(int color, int x1, int y1, int x2, int y2) {
		// System.out.println("Throughhhh Check");

		// initializeboard();

		if (y2 > y1) {

			for (int i = y1 + 1; i <= y2; i++) {
				tempboard = createTempBoard();
				tempKingMove(color, x1, y1, x2, i);
				if (isinCheck(color)) {
					board = tempboard;
					initialkingpos(color);
					return false;
				}
				board = tempboard;
			}

		} else {
			for (int i = y1 - 1; i >= y2; i--) {
				tempboard = createTempBoard();
				tempKingMove(color, x1, y1, x2, i);
				if (isinCheck(color)) {
					// System.out.println("inCheck:" + x2+ i);

					board = tempboard;
					initialkingpos(color);

					return false;
				}
				board = tempboard;

			}

		}

		// System.out.println("true>");
		return true;
	}

	/**
	 * Execute all the valid move of pieces with valid turn
	 */
	public static void ExecuteMove() {
		int x1 = oldcoordinate[0];
		int y1 = oldcoordinate[1];
		int x2 = newcoordinate[0];
		int y2 = newcoordinate[1];

		// if (turn == 0) {

		if (isOccupied(x1, y1) && isValidTurn(x1, y1)) {

			if (isinCheck(turn) && !(board[x1][y1] instanceof King) && !KingProtected(turn, x2, y2)

					&& !BlockChecking(turn, x1, y1, x2, y2)) {

				if (turn == 0) {

					System.out.println("WHITE KING BEING CHECKED!");
					System.out.println("Illegal move ! try again");
					initializeboard();

				} else {
					System.out.println("BLACK KING BEING CHECKED!");
					System.out.println("Illegal move ! try again");
					initializeboard();
				}
				return;
			}
			// System.out.println("piece is :" + board[x1][y1].name + ",and color :" +
			// board[x1][y1].color);

			if (board[x1][y1].isValidMove(board[x1][y1].color, x1, y1, x2, y2)) {

				if (board[x1][y1] instanceof King) {

					if (((King) board[x1][y1]).isCastling(x1, y1, x2, y2)) {
						if (isinCheck(turn)) {

							initializeboard();
							System.out.println("InValid move! Try again");
							return;
						}
						if (!isThroughCheck(board[x1][y1].color, x1, y1, x2, y2)) {
							initializeboard();
							System.out.println("InValid move! Try again");
							return;
						}

						if (isValidRook(board[x1][y1].color, x1, y1, x2, y2)) {
							tempboard = createTempBoard();
							// System.out.println("Castling here");
							MakeCastlingMove(board[x1][y1].color, x1, y1, x2, y2);
							initializeboard();

						}
					} else {
						tempboard = createTempBoard();

						board[x1][y1].Move(board[x1][y1].color, x1, y1, x2, y2);
						if (isinCheck(turn)) {
							board = tempboard;
							if (turn == 0) {
								whitekingpos[0] = x1;
								whitekingpos[1] = y1;

							} else {
								blackkingpos[0] = x1;
								blackkingpos[1] = y1;
							}
							initializeboard();
							System.out.println("InValid move ! Try again");

							return;

						}
					}

				} else {

					tempboard = createTempBoard();

					board[x1][y1].Move(board[x1][y1].color, x1, y1, x2, y2);
					// initializeboard();
					promotion = " ";
					if (isinCheck(board[x2][y2].color)) {
						board = tempboard;
						initializeboard();
						System.out.println("InValid move ! Try again");

						return;

					}

				}
				Check(turn, x2, y2);
				// printposking();
				AlterTurn();
				initializeboard();
				return;

			} else {
				if (board[x1][y1] instanceof Pawn) {

					if (CheckEnPassant(board[x1][y1].color, x1, y1, x2, y2)) {
						//
						// System.out.println("valid en passant");
						Pawn.EnPassant = true;
						board[x1][y1].Move(board[x1][y1].color, x1, y1, x2, y2);
						Check(turn, x2, y2);
						initializeboard();
						AlterTurn();
						return;
					}
				}
				initializeboard();
				System.out.println("Invalid move, Try again!");
				return;
			}

		} else {
			initializeboard();
			System.out.println("Invalid move, Try again!");
			return;
		}

	}

	/**
	 * get x-y coordinate array of piece given a string from user input
	 * 
	 * @param s filerank
	 * @return a array of length 2 , that stores x in the first index and y in the
	 *         second index
	 */
	public static int[] getXY(String s) {
		int[] result = new int[2];
		if (Character.isLetter(s.charAt(0)) && Character.isDigit(s.charAt(1))) {
			int col = AlphaToInt(s.charAt(0));
			if (col == -1) {
				System.out.println("Not in Domain");
				return null;
			}
			int row = Character.getNumericValue(s.charAt(1));
			if (row < 1 || row > 8) {
				System.out.println("Not in Range");
				return null;
			}
			result[0] = 8 - row;
			result[1] = col;

		}
		return result;

	}

	/**
	 * get Coordinate of piece given an array list of string token from the user's
	 * input and execute move within this function
	 * 
	 * @param ar list of string token from user's input
	 */
	public static void GetCoordinate(ArrayList<String> ar) {
		if (ar.get(0).length() != 2 || ar.get(1).length() != 2) {
			System.out.println("Illegal move, Try again!");
			return;
		}

		oldcoordinate = getXY(ar.get(0));
		if (oldcoordinate == null)
			return;
		newcoordinate = getXY(ar.get(1));
		if (newcoordinate == null)
			return;
		/*
		 * if(!promotion.equals(" ")) { if(turn == 0) { if(newcoordinate[0] != 0) {
		 * System.out.println("Illegal move, Try again!"); promotion = " "; return; }
		 * }else { if(newcoordinate[0] != 7) {
		 * System.out.println("Illegal move, Try again!"); promotion = " "; return; } }
		 * }
		 */

		ExecuteMove();
	}

	/**
	 * Break Down input from the user into a move
	 * 
	 * @param input user's input
	 */
	public static void BreakDownInput(String input) {

		if (input == null) {
			System.out.println("illegal input, try again!");
			return;
		}

		StringTokenizer st = new StringTokenizer(input, " ");
		ArrayList<String> ar = new ArrayList<String>();
		while (st.hasMoreTokens()) {
			ar.add(st.nextToken());
		}
		if (ar.size() > 3) {
			System.out.println("Illegal input, try again");
			return;
		}
		if (ar.size() == 1) {
			if (ar.get(0).contains("resign")) {
				if (turn == 0) {
					System.out.println("Black wins");
				} else {
					System.out.println("White wins");
				}
				System.exit(0);
			} else if (ar.get(0).contains("draw")) {
				if (turn == 0 && BlackDraw) {
					System.exit(0);
				} else if (turn == 1 && WhiteDraw) {
					System.exit(0);
				} else {
					System.out.println("Invalid input,Should ask for Draw? if you wanna draw");
				}
			} else {
				System.out.println("Invalid input, try again!");
				return;
			}

		} else if (ar.size() == 2) {

			GetCoordinate(ar);

		} else if (ar.size() == 3) {

			if (ar.get(2).equals("draw?")) {
				SendDraw(turn);
				GetCoordinate(ar);
			} else if (ar.get(2).equals("r") || ar.get(2).equals("n") || ar.get(2).equals("b")
					|| ar.get(2).equals("q")) {
				promotion = ar.get(2);
				// System.out.println("promotion");
				GetCoordinate(ar);

				return;
			} else {
				System.out.println("Invalid Input, Try again!");
				return;
			}
		}

	}

	/**
	 * turn on boolean value when the player wants to draw the game given the its
	 * color
	 * 
	 * @param color player identification
	 */
	public static void SendDraw(int color) {
		if (color == 0) {
			WhiteDraw = true;
		} else {
			BlackDraw = true;
		}
	}

	/**
	 * Check to see if the player's move is in its right turn
	 * 
	 * @param x row
	 * @param y column
	 * @return boolean value corresponding to the turn of the player
	 */
	public static boolean isValidTurn(int x, int y) {
		if (isOccupied(x, y)) {
			if (turn != Board.board[x][y].color) {
				return false;
			}
		}
		return true;
	}

	/**
	 * initialize kingposition in initial state
	 * 
	 * @param color player identification
	 */
	public static void initialkingpos(int color) {
		if (color == 0) {
			whitekingpos[0] = 7;
			whitekingpos[1] = 4;
		} else {
			blackkingpos[0] = 0;
			blackkingpos[1] = 4;
		}
	}

	/**
	 * Checking if the king can move anywhere under checkmate check
	 * 
	 * @param color player identification
	 * @return boolean value corresponding to king's move
	 */
	public static boolean CanKingMove(int color) {
		int x = -1;
		int y = -1;
		if (color == 0) {
			x = whitekingpos[0];
			y = whitekingpos[1];
		} else {
			x = blackkingpos[0];
			y = blackkingpos[1];
		}
		// printposking();

		if ((y + 1) < 8) {
			if (((King) board[x][y]).ValidMove(color, x, y, x, y + 1)) {
				return true;
			}

			if ((x - 1) >= 0) {
				if (((King) board[x][y]).ValidMove(color, x, y, x - 1, y + 1)) {
					return true;
				}
			}

			if ((x + 1) < 8) {
				if (((King) board[x][y]).ValidMove(color, x, y, x + 1, y + 1)) {
					return true;
				}
			}

		}
		if ((y - 1) >= 0) {

			if (((King) board[x][y]).ValidMove(color, x, y, x, y - 1)) {
				return true;
			}

			if ((x - 1) >= 0) {
				if (((King) board[x][y]).ValidMove(color, x, y, x - 1, y - 1)) {
					return true;
				}
			}

			if ((x + 1) < 8) {
				if (((King) board[x][y]).ValidMove(color, x, y, x + 1, y - 1)) {
					return true;
				}
			}

		}
		if ((x - 1) >= 0) {

			if (((King) board[x][y]).ValidMove(color, x, y, x - 1, y)) {
				return true;
			}

		}
		if ((x + 1) < 8) {

			if (((King) board[x][y]).ValidMove(color, x, y, x + 1, y)) {
				return true;
			}

		}
		// printposking();
		// System.out.println("cant move king");
		return false;
	}

	/**
	 * 
	 * Get list of the potential moves to block the opponent from checking the King
	 * 
	 * @param classname class name
	 * @param color     player identification
	 * @param x1        current row
	 * @param y1        current column
	 * @param x2        new row
	 * @param y2        new column
	 * @return an array list of a 2D integer array of length 2, that stores the
	 *         coordinates of the possible path the piece can move to get the king
	 *         out of being checked
	 */
	public static ArrayList<int[]> getPotentialMove(String classname, int color, int x1, int y1, int x2, int y2) {
		ArrayList<int[]> ar = new ArrayList<int[]>();

		if (classname.equals("piece.Queen") || classname.equals("piece.Bishop")) {
			if (Math.abs(x1 - x2) == Math.abs(y1 - y2)) {
				int xoffset;
				int yoffset;
				if (x1 < x2) {
					xoffset = 1;
				} else {
					xoffset = -1;
				}
				if (y1 < y2) {
					yoffset = 1;
				} else {
					yoffset = -1;
				}
				int j = y1 + yoffset;
				for (int i = x1 + xoffset; i != x2; i += xoffset) {
					// System.out.println(i+" "+j);
					int[] num = new int[2];
					num[0] = i;
					num[1] = j;
					ar.add(num);

					j += yoffset;
				}
			}
		} else if (classname.equals("piece.Rook") || classname.equals("piece.Queen")) {
			int offset;
			if (x1 != x2 && y1 == y2) {
				if (x1 < x2) {
					offset = 1;
				} else {
					offset = -1;
				}
				for (int i = x1 + offset; i != x2; i += offset) {
					int[] num = new int[2];
					num[0] = i;
					num[1] = y1;
					ar.add(num);

				}
			}
			if (y1 != y2 && x1 == x2) {
				if (y1 < y2) {
					offset = 1;

				} else {
					offset = -1;
				}
				for (int i = y1 + offset; i != y2; i += offset) {
					int[] num = new int[2];
					num[0] = x1;
					num[1] = i;
					ar.add(num);

				}

			}
		}

		return ar;

	}

	/**
	 * Check to see if the piece can block the Opponent from checking its King
	 * 
	 * @param color player identification
	 * @param x1    current row
	 * @param y1    current column
	 * @param x2    new row
	 * @param y2    new column
	 * @return boolean value corresponding to the moves of pieces
	 */
	public static boolean CanBlockTheCheckingPiece(int color, int x1, int y1, int x2, int y2) {
		String classname = board[x1][y1].getClass().getName();
		if (!(classname.equals("piece.Rook") || classname.equals("piece.Queen") || classname.equals("piece.Bishop"))) {
			return false;
		}
		ArrayList<int[]> tmp = getPotentialMove(classname, color, x1, y1, x2, y2);
		if (tmp.size() <= 0) {
			return false;
		}
		/*
		 * for (int i = 0; i < tmp.size(); i++) { System.out.println(tmp.get(i)[0] + ","
		 * + tmp.get(i)[1]); }
		 */
		for (int i = 0; i < tmp.size(); i++) {
			for (int j = 0; j < 8; j++) {
				for (int w = 0; w < 8; w++) {
					if (isOccupied(j, w)) {
						if (board[j][w].color == color) {
							if (board[j][w].isValidMove(color, j, w, tmp.get(i)[0], tmp.get(i)[1])
									&& !(board[j][w] instanceof King)) {

								// System.out.println(j + " " + w);
								return true;
							}
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * Check if any pieces can move to help the king from being checked
	 * 
	 * @param color player identification
	 * @return boolean value corresponding to whether the piece's move can help the
	 *         king from being checked
	 */
	public static boolean CanAnyPieceMove(int color) {
		int Opx = -1;
		int Opy = -1;
		int kx = -1;
		int ky = -1;
		if (color == 0) {
			Opx = BlackChecking[0];
			Opy = BlackChecking[1];
			kx = whitekingpos[0];
			ky = whitekingpos[1];

		} else {
			Opx = WhiteChecking[0];
			Opy = WhiteChecking[1];
			kx = blackkingpos[0];
			ky = blackkingpos[1];
		}
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (isOccupied(i, j)) {
					if (board[i][j].color == color) {
						if (board[i][j].isValidMove(color, i, j, Opx, Opy) && !(board[i][j] instanceof King)) {

							// System.out.println("Movepice: " + i + "," + j);
							return true;
						}
					}
				}
			}
		}

		if (CanBlockTheCheckingPiece(color, Opx, Opy, kx, ky)) {
			return true;
		}

		return false;
	}

	/**
	 * Check if the situation is a Checkmate by checking if the king can move or
	 * other pieces can move to help the king from being checked
	 * 
	 * @param color player identification
	 * @return true if check mate , false otherwise
	 */
	public static boolean isCheckMate(int color) {
		if (!CanKingMove(color) && !CanAnyPieceMove(color)) {

			return true;

		}
		return false;
	}

	/**
	 * Check to see if any pieces can move anywhere anymore
	 * 
	 * @param color indicates player's turn
	 * @return boolean value corresponding to all possible move of pieces
	 */
	public static boolean isStaleMate(int color) {

		tempboard = createTempBoard();
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				for (int k = 0; k < 8; k++) {
					for (int w = 0; w < 8; w++) {
						try {
							if (isOccupied(i, j)) {
								if (board[i][j].color == color) {
									if (board[i][j].isValidMove(color, i, j, k, w) && !(board[i][j] instanceof King)) {
										board[i][j].Move(color, i, j, k, w);
										if (!isinCheck(color)) {
											// initializeboard();
											// System.out.println("move:" + i+","+ j+" "+k +"," + w);
											return false;
										}
									}
									board = tempboard;
								}
							}
							board = tempboard;

						} catch (Exception e) {
							board = tempboard;
						}
					}
				}
			}
		}
		board = tempboard;
		return true;
	}

	

}
