package chessgame;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;


import piece.Piece;
/**
 * The place where you start the game
 * @author linhdang

 */
 //MJ
public class GameChess {
	
/**
 * Starts the Game
 * @param args this is not going to be used
 * @throws IOException throw exception for giving invalid input
 */
	public static void main(String[] args) throws IOException {

		boolean start = true;
		Board.initializeboard();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		Board.whitekingpos[0] = 7;
		Board.whitekingpos[1] = 4;
		Board.blackkingpos[0] = 0;
		Board.blackkingpos[1] = 4;
		/*try {
			BufferedReader reader = new BufferedReader(
					new FileReader("/Users/linhdang/Documents/Software Meth/promotion.txt"));
			String line = reader.readLine();
			line = reader.readLine();
			// BreakDownInput(line.toLowerCase());
			while (line != null) {
				System.out.println(line);
				Board.BreakDownInput(line.toLowerCase());
				line = reader.readLine();

			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		String input = null;
		while (start) {

			input = null;

			Piece[][] oldboard = Board.createTempBoard();
			if (Board.isinCheck(Board.turn)) {
				
				if (Board.isCheckMate(Board.turn)) {
					System.out.println("CheckMate");
					if (Board.turn == 0) {
						System.out.println("Black Wins!");
					} else {
						System.out.println("White Wins!");
					}
					System.exit(0);
				}

			} else if (Board.isStaleMate(Board.turn) && !Board.CanKingMove(Board.turn)) {
				System.out.println("Stalemate");
				System.exit(0);
			}

			if (Board.turn == 0) {
				System.out.print("White's move:");

			} else if (Board.turn == 1) {
				System.out.print("Black's move: ");

			}

			Board.board = oldboard;
			input = br.readLine();
			Board.BreakDownInput(input.toLowerCase());

		}
	}
}
