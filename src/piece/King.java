package piece;

import chessgame.Board;
/**
 * King class 
 * @author linhdang
 *
 */
 //Maury Johnson
public class King extends Piece {
	/**
	 * Indicates if the black king has moved
	 */
	public static boolean Bkingmove = false;
	/**
	 * Indicates if the white king has moved
	 */
	public static boolean Wkingmove = false;
/**
 * King constructor
 * @param color player's turn
 * @param name piece name
 */
	public King(int color, String name) {
		super(color, name);
	}

	/**
	 * Mark boolean value when the king has moved
	 * 
	 * @param color indicates player's turn
	 */
	public void initialpos(int color) {
		if (color == 0)
			Wkingmove = true;
		if (color == 1)
			Bkingmove = true;

	}

	/**
	 * Undo the position of the king for some temporary move
	 * 
	 * @param color indicates player's turn
	 * @param x current row
	 * @param y current column
	 */
	public void UndoKingPos(int color, int x, int y) {
		if (color == 0) {
			Board.whitekingpos[0] = x;
			Board.whitekingpos[1] = y;

		} else {
			Board.blackkingpos[0] = x;
			Board.blackkingpos[1] = y;
		}
	}

	/**
	 * This is a temporary ValidMove for testing CheckMate
	 * 
	 * @param color indicates player's turn
	 * @param x1 current row
	 * @param y1 current column
	 * @param x2 new row
	 * @param y2 new column
	 * @return boolean value indicates if its a valid move under checkmate test
	 */
	public boolean ValidMove(int color, int x1, int y1, int x2, int y2) {
		Board.tempboard = Board.createTempBoard();
		if (isValidMove(color, x1, y1, x2, y2)) {
			TempMove(color, x1, y1, x2, y2);
		} else {
			return false;
		}

		if (Board.isinCheck(color)) {
			Board.board = Board.tempboard;
			UndoKingPos(color, x1, y1);
			

			return false;
		}
		UndoKingPos(color, x1, y1);
		return true;
	}

	/**
	 * Checking if the king is trying to do valid Castling
	 * 
	 * @param x1 current row
	 * @param y1 current column
	 * @param x2 new row
	 * @param y2 new column
	 * @return boolean value indicates if you can castle
	 */
	public boolean isCastling(int x1, int y1, int x2, int y2) {
		if (Math.abs(y1 - y2) == 2 && x1 == x2 && y2 > y1) {
			if (Board.isOccupied(x1, y1 + 1) || Board.isOccupied(x1, y1 + 2)) {
				return false;
			}
		} else if (Math.abs(y1 - y2) == 2 && x1 == x2 && y1 > y2) {
			if (Board.isOccupied(x1, y1 - 1) || Board.isOccupied(x1, y1 - 2) || Board.isOccupied(x1, y1 - 3)) {
				return false;
			}

		} else {
			return false;
		}
		return true;
	}

	/**
	 * Temporary King move for testing Checkmate
	 * 
	 * @param color indicates player's turn
	 * @param x1 current row
	 * @param y1 current column
	 * @param x2 new row
	 * @param y2 new column
	 */
	public void TempMove(int color, int x1, int y1, int x2, int y2) {
		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1, colortile);
		if (color == 0) {
			Board.board[x2][y2] = new King(0, " wK");

		} else {
			Board.board[x2][y2] = new King(1, " bK");
		}

		if (color == 0) {
			Board.whitekingpos[0] = x2;
			Board.whitekingpos[1] = y2;

		} else {
			Board.blackkingpos[0] = x2;
			Board.blackkingpos[1] = y2;
		}
	}

	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {
		if (!(Math.abs(x1 - x2) <= 1 && Math.abs(y1 - y2) <= 1)) {

			if (color == 0 && Wkingmove == true) {

				return false;
			}

			if (color == 1 && Bkingmove == true) {
				return false;
			}
			if (!isCastling(x1, y1, x2, y2)) {
				return false;
			}
		}
		if (Board.board[x2][y2].color == color)
			return false;
		return true;
	}

	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {

		if (Bkingmove == false || Wkingmove == false) {

			initialpos(color);
		}

		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1, colortile);
		if (color == 0) {
			Board.board[x2][y2] = new King(0, " wK");

		} else {
			Board.board[x2][y2] = new King(1, " bK");
		}

		if (color == 0) {
			Board.whitekingpos[0] = x2;
			Board.whitekingpos[1] = y2;

		} else {
			Board.blackkingpos[0] = x2;
			Board.blackkingpos[1] = y2;
		}

	}

}
