package piece;
//import chessgame.*;
/**
 * Abstract Piece Class
 * @author linhdang
 *
 */
public abstract class Piece {
	/**
	 * Piece name
	 */
	public String name;
	/**
	 * Player's turn
	 */
	public int color;
/**
 * color indicates if the piece is white or black
 * name indicates name of the piece
 * @param color indicates player's turn
 * @param name name of pieces
 */
	public Piece(int color,String name) {
		
		this.name = name;
		this.color = color;
	}
	/**
	 * Check Valid move of pieces
	 * @param color player's turn
	 * @param x1 current row
	 * @param y1 current column
	 * @param x2 new row
	 * @param y2 new column
	 * @return true if the move is valid
	 */
	public abstract boolean isValidMove(int color, int x1, int y1, int x2, int y2);
	/**
	 * Execute move given color, current position and new position
	 * @param color player's turn
	 * @param x1 current row
	 * @param y1 current column
	 * @param x2 new row
	 * @param y2 new column
	 */
	public abstract void Move(int color, int x1, int y1, int x2, int y2);
	
	
}
