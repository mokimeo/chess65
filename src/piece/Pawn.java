package piece;

import chessgame.Board;
/**
 * Pawn Class
 * @author linhdang
 *
 */
public class Pawn extends Piece {
	/**
	 * boolean value indicates if piece can do en passant
	 */
	public static boolean EnPassant = false;
	/**
	 * Pawn constructor
	 * @param color player's turn
	 * @param name piece name
	 */
	public Pawn(int color, String name) {
		super(color, name);

	}
	/**
	 * Check to see if the Pawn has already moved
	 * @param color player's turn
	 * @param x1 current row
	 * @return boolean value corresponding to initial piece position
	 */
	public boolean initialpiece(int color, int x1) {
		if (color == 0 && x1 == 6)
			return true;
		if (color == 1 && x1 == 1)
			return true;
		return false;
	}

	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {

		if (color == 0) {
			if (x1 <= x2)
				return false;
		} else {
			if (x1 >= x2)
				return false;
		}
		if (y1 == y2) {
			
			if(color == 0) {
				if(Board.isOccupied(x1 - 1, y1)) {
					return false;
				}
			}else {
				if(Board.isOccupied(x1 + 1, y1)) {
					return false;
				}
			}
			if (Math.abs(x1 - x2) > 2)
				return false;
			if (Math.abs(x1 - x2) == 2) {
					if (!initialpiece(color, x1)) {
						return false;
					}else {
						//System.out.println("yes");
						int[] tempar = new int[2];
						tempar[0] = x2;
						tempar[1] = y2;
						Board.listap.add(tempar);
					}
					
	
						
					
					
				
				if (color == 0) {
	
					
	
					if (Board.isOccupied(x1 - 2, y1))
						return false;
	
				} else {
					
	
					if (Board.isOccupied(x1 + 2, y1))
						return false;
				}
			}	
		} else {
			if (Math.abs(y2 - y1) != 1 || Math.abs(x2 - x1) != 1) {
				return false;
			}
			if (!Board.isOccupied(x2, y2)) {
				return false;
			}
		}
		if(Board.board[x2][y2].color == color) return false;
		return true;
	}
	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {

		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1, colortile);
		
		if (color == 0) {
			if (x2 == 0) {

				switch (Board.promotion.charAt(0)) {
				case 'r':
					Board.board[x2][y2] = new Rook(0, " wR");
					break;
				case 'n':
					Board.board[x2][y2] = new Knight(0, " wN");
					break;
				case 'b':
					Board.board[x2][y2] = new Bishop(0, " wB");
					break;
				case 'q':
					Board.board[x2][y2] = new Queen(0, " wQ");
					break;
				default:
					Board.board[x2][y2] = new Queen(0, " wQ");
					break;
				}
				return;
			}
			
		} else {
			if (x2 == 7) {	
				System.out.println(Board.promotion);
				switch (Board.promotion.charAt(0)) {
				case 'r':
					Board.board[x2][y2] = new Rook(1, " bR");
					break;
				case 'n':
					Board.board[x2][y2] = new Knight(1, " bN");
					break;
				case 'b':
					Board.board[x2][y2] = new Bishop(1, " bB");
					break;
				case 'q':
					Board.board[x2][y2] = new Queen(1, " bQ");
					break;
				default:
					Board.board[x2][y2] = new Queen(1, " bQ");
					break;
				}
				return;

			}
			

		}
		if (color == 0) {
			Board.board[x2][y2] = new Pawn(0, " wP");

		} else {
			Board.board[x2][y2] = new Pawn(1, " bP");
		}
		if (EnPassant == true) {
			if (Board.isOccupied(x2 - 1, y2) && (Board.board[x2 - 1][y2] instanceof Pawn) && color != Board.board[x2 - 1][y2].color && color == 1) {
				colortile = Board.getcolor(x2 - 1, y2);
				Board.board[x2 - 1][y2] = new EmptyPiece(-1, colortile);
				EnPassant = false;
			} else if (Board.isOccupied(x2 + 1, y2)  && (Board.board[x2 + 1][y2] instanceof Pawn) && color != Board.board[x2 + 1][y2].color && color == 0) {
				colortile = Board.getcolor(x2 + 1, y2);
				Board.board[x2 + 1][y2] = new EmptyPiece(-1, colortile);
				EnPassant = false;
			}

		}

	}
}
