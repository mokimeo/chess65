package piece;

import chessgame.Board;
/**
 * Knight class
 * @author linhdang
 *
 */
public class Knight extends Piece {
	/**
	 * Knight Constructor
	 * @param color player's turn
	 * @param name piece name
	 */
	public Knight(int color,String name) {
		super(color, name);
	}

	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {
		
		if(Math.abs(x1 - x2) == 2 && Math.abs(y1 - y2) == 1) {
			if(Board.board[x2][y2].color == color) return false;
			return true;
		}
		if(Math.abs(x1 - x2) == 1 && Math.abs(y2 - y1) == 2) {
			if(Board.board[x2][y2].color == color) return false;
			return true;
		}
		
		return false;
	}

	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {
		
		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1,colortile);
		if(color == 0) {
			Board.board[x2][y2] = new Knight(0," wN");

			
		}else {
			Board.board[x2][y2] =new Knight(1," bN");
		}
	}
}
