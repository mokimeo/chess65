package piece;

import chessgame.Board;
/**
 * Rook class 
 * @author linhdang
 *
 */
public class Rook extends Piece {
	/**
	 * Right White Rook position
	 */
	public static boolean RWRookMoved = false;
	/**
	 * Left White Rook position
	 */
	public static boolean LWRookMoved = false;
	/**
	 * Right Black Rook position
	 */
	public static boolean RBRookMoved = false;
	/**
	 * Left Black Rook position
	 */
	public static boolean LBRookMoved = false;
	/**
	 * Rook constructor
	 * @param color player's turn
	 * @param name piece name
	 */
	public Rook(int color,String name) {
		
		super(color, name);
	}
	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {
		if(!(x1 == x2 || y1 == y2)) return false;
		if(x1 == x2) {
			if(y1 == y2) return false;
			if(y1 < y2) {
				for(int i = y1 + 1; i < y2; i++) {
					if(Board.isOccupied(x1, i) ) {
						return false;
					}
				}
				
				
			}else {
				for(int i = y1 - 1; i > y2; i--) {
					if(Board.isOccupied(x1, i)) {
						return false;
					}
				}
			}
		
		}
		if(y1 == y2) {
			if(x1 == x2) return false;
			if(x1 > x2) {
				for(int i = x1 - 1; i > x2; i--) {
					if(Board.isOccupied(i, y1)) {
						return false;
					}
				}
			}else {
				for(int i = x1 + 1; i < x2; i++) {
					if(Board.isOccupied(i, y1)) {
						return false;
					}
				}
			}
		}
		if(Board.board[x2][y2].color == color) return false;
		
		return true;
	}

	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {
		if(color == 0) {
			if(x1 == 7 && y1 == 0 && LWRookMoved == false) {
				LWRookMoved = true;
			}else if(x1 == 7 && y1 == 7 && RWRookMoved == false) {
				RWRookMoved = true;
			}
		}else {
			if(x1 == 0 && y1 == 0 && LBRookMoved == false) {
				LBRookMoved = true;
			}else if (x1 == 0 && y1 == 7 & RBRookMoved == false) {
				RBRookMoved = true;
			}
		}
		String colortile = Board.getcolor(x1, y1);
		Board.board[x1][y1] = new EmptyPiece(-1,colortile);
		if(color == 0) {
			Board.board[x2][y2] = new Rook(0," wR");

			
		}else {
			Board.board[x2][y2] =new Rook(1," bR");
		}

		
	}
}
