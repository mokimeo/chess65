package piece;

public class EmptyPiece extends Piece {
/**
 * Empty piece class
 * @param color player's turn
 * @param name Piece name
 */
	public EmptyPiece(int color,String name) {
		super(color, name);
		
	}
	@Override
	public boolean isValidMove(int color, int x1, int y1, int x2, int y2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void Move(int color, int x1, int y1, int x2, int y2) {
		// TODO Auto-generated method stub
		
	}

}
